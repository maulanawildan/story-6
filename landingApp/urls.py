from django.urls import path
from . import views
from .views import *
from django.conf.urls import url

app_name = 'landingApp'

urlpatterns = [
    path('', views.landingPage, name='landingPage'),
    url(r'(?P<pk>[0-9]+)/delete/$', views.DeleteStatus.as_view(), name='delete'),
    path('tambah_status/',tambah_status, name='tambah_status'),
]