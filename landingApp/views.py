from django.shortcuts import render, redirect
from .forms import PostForm
from .models import Status
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy

class DeleteStatus(DeleteView):
        model = Status
        success_url = reverse_lazy('landingApp:landingPage')

def landingPage(request):

    context = {
        'post_form' : PostForm(),
        'posting': Status.objects.all() 
    }
    return render(request, 'landingPage.html',context)

def tambah_status(request):
    if PostForm(request.POST).is_valid():
        status_baru = Status.objects.create(status=request.POST['status'])
    return redirect('landingApp:landingPage')

    